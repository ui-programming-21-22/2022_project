﻿// JavaScript source code for html page Game

// 1200 900 is screen



let direction = 5;
let finnSpeed = 5;
let cellX = 0;
let cellY = 0;

let pointerRingRot = 0;

let walkCycle = 0;
let walkCount = 0;

let fruitX = 0;
let fruitY = 0;

let fruitRow = 0;
let fruitCol = 0;

let startTime = 60 * 10;


let gameOver = false;

const canvas = document.getElementById("the_canvas");

var myFont = new FontFace('myFont', 'url(ASSETS/FONTS/ogFont.ttf)');


let goundPosY = [600, 650, 700, 750, 800, 850, 900];
let goundPosX = [0, 0, 0, 0, 0, 0, 0];

let foreHeigth = -1200;
let otherFore = 0;

let noOfCharacters = 5;
let charX = [1200, -400, 2000, 1200, 1200];
let charY = [300, 400, -400, 300, 300];



//stored items
let currentTime = 1;
let jiX = 0;
let jiY = 0;
let previousCharacter = -1;
let currentCharacter = 0;






//local starage
//if (typeof (Storage) !== "undefined") {

//	if (localStorage.getItem("currentCar") == "undefined") {


//		localStorage.setItem("currentCar", 0);
//		localStorage.setItem("jiXHold", 0);
//		localStorage.setItem("jiYHold", 0);
//		localStorage.setItem("timeLeft", 1);
//	} else {
//		let stringHold = "";
//		stringHold = "Do you want to continue? You were on level: ";
//		stringHold += localStorage.getItem("currentCar");
//		stringHold += ".";

//		let confirmAction = confirm(stringHold);
//		if (confirmAction) {
//			currentCharacter = localStorage.getItem("currentCar");
//			jiX = localStorage.getItem("jiXHold");
//			jiY = localStorage.getItem("jiYHold");
//			currentTime = localStorage.getItem("timeLeft");
//		} else {
//			localStorage.setItem("currentCar", 0);
//			localStorage.setItem("jiXHold", 0);
//			localStorage.setItem("jiYHold", 0);
//			localStorage.setItem("timeLeft", 1);

//		}
//	}
//} else
//{
//		console.log("no storage");
//}




myFont.load().then(function (font) {
	document.fonts.add(font);
	console.log('Font loaded');
});

let textForPeople =
	[
		[
			"Finn: Hello!",
			//temp
			"newChoose",
			"0",
			"Finn: I'm Finn, What's your name?",
			"...",
			"Finn: Jikanji?",
			"Finn: Cool name!",
			"...",
			"Finn: What am I doing here?",
			"Finn: I'm just looking at the stars...",
			"...",
			"Finn: What's that?",
			"Finn: You need a fragment to complete the game?",
			"Finn: And you're against the clock?",
			"Finn: I don't have it yet.",
			"Finn: Try asking Ninya.",
			"Finn: I know you don't have much time here...",
			"Finn: But, remember don't rush",
			"next",
			"1"
		]
		,
		[
			"Ninya: I'm busy!",
			"Ninya: What do you want?",
			"...",
			"Ninya: A fragment???",
			"Ninya: Don't have one.",
			"Ninya: I could never afford one...",
			"Ninya: Ask the sea king, he's rich!",
			"Ninya: Or you could just go back to Finn",
			"Ninya: and wait out your time here",
			"newChoose",
			"1"
		]
		,
		[
			"Hello",
			"Go to Prim",
			"next",
			"4"
		]
		,
		[
			"Finn sleeps peacefully",
			"...",
			"exit"
		]
		,
		[
			"Prim: Hello",
			"...",
			"Prim: Bye",
			"exit"
		]
	];


let charAudio =
	[
		new Audio('ASSETS/SOUNDS/jump_sound.wav'),
		new Audio('ASSETS/SOUNDS/jump_sound.wav'),
		new Audio('ASSETS/SOUNDS/jump_sound.wav'),
		new Audio('ASSETS/SOUNDS/jump_sound.wav'),
		new Audio('ASSETS/SOUNDS/jump_sound.wav')
	];

let currentTalkSentence = 0;
let textSlowed = 0;
let textPart = 0;
let talking = false;
let talkingdir = 0;


let choosing = false;
let chooseArrays = 0;
let chooseHold =
	[
		[
			"Bye!",
			"exit",
			"Hello!",
			"next"
		]
		,
		[
			"sea king",
			"2",
			"finn",
			"3"
		]
	]

let currentOption = 0;
function chooseFunc(t_hold) {
	currentOption += t_hold;
	if (currentOption < 0) {
		currentOption = chooseHold[chooseArrays].length / 2 - 1;
	}
	if (currentOption > chooseHold[chooseArrays].length / 2 - 1) {
		currentOption = 0;
	}
	if (t_hold == 0) {
		choosing = false;
		if ("exit" == (chooseHold[chooseArrays][currentOption * 2 + 1])) {
			window.close();
		}
		else if ("next" == (chooseHold[chooseArrays][currentOption * 2 + 1])) {
			talking = true;
		}
		else {
			previousCharacter = currentCharacter;
			currentCharacter = (chooseHold[chooseArrays][currentOption * 2 + 1]);
			currentTalkSentence = 0;
        }

    }
	console.log(t_hold)

}

let audionew = new Audio('ASSETS/SOUNDS/Veil.wav');


var joystick = nipplejs.create({
	color: 'red',
	zone: document.getElementById('the_canvas')
});

joystick.on('dir:right', function (evt, data) {
	direction = 3;
	cellX = 3;
});
joystick.on('dir:up', function (evt, data) {
	direction = 0;
	cellX = 1;
});
joystick.on('dir:left', function (evt, data) {
	direction = 2;
	cellX = 2;
});
joystick.on('dir:down', function (evt, data) {
	direction = 1;
	cellX = 0;
});
joystick.on('start end', function (evt, data) {
	walkCount = 0;
	direction = 4;
});


function input(event) {
	if (event.type === "keydown") {
		switch (event.keyCode) {
			case 37:
				//console.log("left arrow pressed");
				if (choosing) {
					chooseFunc(-1);
				}
				else {
					direction = 2;
					cellX = 2;
				}
				break;
			case 38:
				//console.log("up arrow pressed");
				if (!choosing) {
					direction = 0;
					cellX = 1;
				}
				break;
			case 39:
				//console.log("right arrow pressed");
				if (choosing) {
					chooseFunc(1);
				}
				else {
					direction = 3;
					cellX = 3;
				}
				break;
			case 40:
				//console.log("down arrow pressed");
				if (!choosing) {
					direction = 1;
					cellX = 0;
				}
				break;
			case 189:
				console.log("- pressed");
				//cellY++;
				//if (cellY > 2) {
				//	cellY = 0;
				//  } 
				break;
			case 187:
				console.log("+ pressed");
				//cellY--;
				//if (cellY < 0) {
				//	cellY = 2;
				//}
				break;
			case 32:
				console.log("Space");
				let audio = new Audio('ASSETS/SOUNDS/explode1.ogg');
				audio.play();
				break;
			case 13:
				console.log("Enter");
				//audionew.pause();
				if (choosing) {
					chooseFunc(0);
				}
				else {
					talk();
				}
				break;

			default:
				direction = 4;
				walkCount = 0;
		}

	}
	if (event.type === "keyup") {
		walkCount = 0;
		direction = 4;
	}
}

function talk() {

	if (!choosing) {
		if (300 > Math.sqrt(
			((charX[currentCharacter] - jiX) + 256 - 600) *
			((charX[currentCharacter] - jiX) + 256 - 600) +
			((charY[currentCharacter] - jiY) + 256 - 700) *
			((charY[currentCharacter] - jiY) + 256 - 700))) {

			if (!talking) {
				talking = true;
			}
			else {
				if (currentTalkSentence < (textForPeople[currentCharacter].length - 1)) {
					currentTalkSentence++;

					if (textForPeople[currentCharacter][currentTalkSentence] == "newChoose") {
						console.log("newChoose!")
						talking = false;
						//currentCharacter++;
						choosing = true;
						chooseArrays = textForPeople[currentCharacter][currentTalkSentence + 1];
						currentTalkSentence += 2;

					}
					if (textForPeople[currentCharacter][currentTalkSentence] == "next") {
						console.log("next person!")
						previousCharacter = currentCharacter;
						currentCharacter = textForPeople[currentCharacter][currentTalkSentence + 1];
						currentTalkSentence = 0;
						talking = false;

					}
					if (textForPeople[currentCharacter][currentTalkSentence] == "exit") {
						window.close();
					}
				}
				else {
					talking = false;
					currentTalkSentence = 0;
				}


				textPart = 0;
			}


		}
		else {

		}
	}


}

function move() {
	switch (direction) {
		case 0:
			walkCount++;
			jiY -= finnSpeed;

			for (let i = 1; i < 7; i++) {
				//goundPosY[i] += 0.2;
			}
			break;
		case 1:
			walkCount++;
			jiY += finnSpeed;
			for (let i = 1; i < 7; i++) {
				//goundPosY[i] -= 0.2;
			}
			break;
		case 2:
			walkCount++;
			jiX -= finnSpeed;
			for (let i = 1; i < 7; i++) {
				goundPosX[i] += (goundPosY[i] - 600) / 10;
			}
			break;
		case 3:
			walkCount++;
			jiX += finnSpeed;
			for (let i = 1; i < 7; i++) {
				goundPosX[i] -= (goundPosY[i] - 600) / 10;
			}
			break;
		default:
	}
}

function buttonRight()
{
	direction = 3;
	cellX = 3;
}
function buttonLeft() {
	direction = 2;
	cellX = 2;
}
function buttonUp() {
	direction = 0;
	cellX = 1;
}
function buttonDown() {
	direction = 1;
	cellX = 0;
}
function realese() {
	walkCount = 0;
	direction = 4;
}

function renderEnd() {
	let context = canvas.getContext("2d");

	context.fillStyle = "rgb(56, 62, 57)";
	context.fillRect(0, 0, canvas.width, canvas.height);

	let image = document.getElementById("the_image");
	context.drawImage(image, walkCycle * 256 + 0 * 256, cellY * 256, 256, 256, 600 - 128, 700 - 128, 256, 256);
}

function render(){
	let context = canvas.getContext("2d");
	context.fillStyle = "rgb(56, 62, 57)";
	context.fillRect(0, 0, canvas.width, canvas.height);
	if (gameOver) {

		context.fillStyle = "rgb(0,0,0)";
		clockMessage = "DEAD!";

		context.fillText(clockMessage, 10, 90);
	}
	else {



		// sky gradient
		var grd2 = context.createLinearGradient(0, 600, 0, 0);
		grd2.addColorStop(1, "rgb(60,0,133,205)");
		grd2.addColorStop(0, "rgb(0, 0, 0,0)");
		context.fillStyle = grd2;
		context.fillRect(0, 0, 1200, 600);





		context.fillStyle = "rgb(255, 225, 225)";
		context.fillRect(150, 20, (canvas.width - 170) - (canvas.width - 170) / (startTime / currentTime), 20);


		context.font = "20px myFont";

		clockMessage = "Time: " + (startTime - currentTime);

		context.fillText(clockMessage, 10, 37);


		//testing stuff........................................................................................................
		//context.fillText(jiX + "," + jiY, 10, 70);
		//context.fillText("current:" + currentCharacter, 10, 105);
		//context.fillText("chooseArrays:" + chooseArrays, 10, 140);
		//context.fillText("currentTalkSentence:" + currentTalkSentence, 10, 185);
		//context.fillText("currentOption:" + currentOption, 10, 230);




		//draw ground
		let groundImage = document.getElementById("the_ground");

		if (goundPosY[1] < 600) {
			for (let i = 1; i < 7; i++) {
				goundPosY[i] += 50;
			}
		}
		if (goundPosY[6] > 850) {
			for (let i = 1; i < 7; i++) {
				goundPosY[i] -= 50;
			}
		}


		for (let i = 1; i < 7; i++) {
			let tempScale = 0;
			tempScale = (goundPosY[i] - 600);
			tempScale /= 50.0;
			if (tempScale == 0) {
				tempScale += 0.1;
			}
			if (goundPosX[i] < -1200) {
				goundPosX[i] += 1200;
			}
			if (goundPosX[i] > 0) {
				goundPosX[i] -= 1200;
			}
			if (i == 4) {


			}
			//context.drawImage(groundImage, 0, 0, 2400, 50, goundPosX[i], goundPosY[i], 2400 * tempScale, 50 * tempScale);


			let testingY = (-jiY % 50);
			testingY += 600;
			//console.log(testingY);
			testingY = testingY + i * 50
			let newScale = (testingY - 600);
			if (newScale < 0.1) {
				newScale = 0.1;
            }
			newScale = newScale / 300;
			newScale *= 2;
			newScale++;


			let offsetX = (2400 * newScale) / 2.0;

			context.drawImage(groundImage, 0, 0, 2400, 50, goundPosX[i] , testingY, 2400 * newScale, 50 * newScale);
		}
		

		// ground gradient
		var grd = context.createLinearGradient(0, 600, 0, 900);
		grd.addColorStop(0, "rgb(56, 62, 57)");
		grd.addColorStop(1, "rgb(0,0,0,0)");
		context.fillStyle = grd;
		context.fillRect(0, 600, 1200, 300);

		//draw ring
		context.strokeStyle = "rgba(232,179,42,50)";
		context.lineWidth = 5;
		context.beginPath();
		context.arc(600, 700, 100, ((pointerRingRot + 330) * Math.PI / 180), ((pointerRingRot + 30) * Math.PI / 180), false);
		context.stroke();

		if (talking) {
			if (((charX[currentCharacter] - jiX)) > 600 - 256) {
				talkingdir = 1;
			}
			else {
				talkingdir = 2;
            }
		}
		else {
			talkingdir = 0;
        }

		//draw characters behind jiji
		if (previousCharacter != -1) {
			if ((charY[(previousCharacter)] - jiY) - 380 < 0) {
				let tempTrans = 0.5;
				if (charY[(previousCharacter)] - jiY < 150) {
					tempTrans = 0;
				}
				else if (charY[(previousCharacter)] - jiY > 250) {
					tempTrans = 1;
				}
				else {
					tempTrans = ((charY[(previousCharacter)] - jiY) - 150) / 100.0;
                }
				context.globalAlpha = tempTrans;
				let imagetemp = document.getElementById("character" + (previousCharacter));
				context.drawImage(imagetemp, 512 * talkingdir, 0, 512, 512, charX[(previousCharacter)] - jiX, charY[(previousCharacter)] - jiY, 512, 512);
				context.globalAlpha = 1;
			}
        }
		if ((charY[currentCharacter] - jiY) - 380 < 0) {
			let tempTrans = 0.5;
			if (charY[currentCharacter] - jiY < 150) {
				tempTrans = 0;
			}
			else if (charY[currentCharacter] - jiY > 250) {
				tempTrans = 1;
			}
			else {
				tempTrans = ((charY[currentCharacter] - jiY) - 150) / 100.0;
			}
			context.globalAlpha = tempTrans;
			let imagetemp = document.getElementById("character" + currentCharacter);
			context.drawImage(imagetemp, 512 * talkingdir, 0, 512, 512, charX[currentCharacter] - jiX, charY[currentCharacter] - jiY, 512, 512);
			context.globalAlpha = 1;
		}

		//draw Jikanji
		let image = document.getElementById("the_image");
		context.drawImage(image, walkCycle * 256 + cellX * 256, cellY * 256, 256, 256, 600 - 128, 700 - 128, 256, 256);

		//draw characters infront jiji
		if (previousCharacter != -1) {
			if ((charY[previousCharacter] - jiY) - 380 >= 0) {
				let imagetemp = document.getElementById("character" + (previousCharacter));
				context.drawImage(imagetemp, 512 * talkingdir, 0, 512, 512, charX[(previousCharacter)] - jiX, charY[(previousCharacter)] - jiY, 512, 512);
			}
		}
		if ((charY[currentCharacter] - jiY) - 380 >= 0) {
			let imagetemp = document.getElementById("character" + currentCharacter);
			context.drawImage(imagetemp, 512 * talkingdir, 0, 512, 512, charX[currentCharacter] - jiX, charY[currentCharacter] - jiY, 512, 512);
		}


		context.fillStyle = "rgba(0,0,0,0.3)";
		context.fillRect(0, 0, canvas.width, canvas.height);



		//dots
		if (300 > Math.sqrt(
			((charX[currentCharacter] - jiX) + 256 - 600) *
			((charX[currentCharacter] - jiX) + 256 - 600) +
			((charY[currentCharacter] - jiY) + 256 - 700) *
			((charY[currentCharacter] - jiY) + 256 - 700))) {

			let imageff = document.getElementById("the_talk");
			context.drawImage(imageff, 0, 0, 256, 256, 600 - 128, 700 - 128, 256, 256);
		}






		//draw dusty
		let forimage = document.getElementById("the_fore");
		foreHeigth += 0.5;
		if (foreHeigth > 0) {
			foreHeigth = -1200;
		}
		otherFore -= 0.5;
		if (otherFore < -900) {
			otherFore = 0;
		}
		context.drawImage(forimage, 0, 0, 2400, 1800, foreHeigth, otherFore, 2400, 1800);



		//choosing box
		if (choosing) {
			context.fillStyle = "rgba(0,0,0,0.4)";
			context.fillRect(50, 750, 1100, 100);

			let stringhold = "";
			for (let i = 0; i < chooseHold[chooseArrays].length / 2; i++) {
				stringhold = chooseHold[chooseArrays][i * 2];
				context.fillStyle = "Gray";
				if (currentOption == i) {
					context.fillStyle = "White";
				}
				context.fillText(stringhold, 100 + 300 * i, 820);

			}


		}


		//talking box
		if (talking) {
			context.fillStyle = "rgba(0,0,0,0.4)";
			context.fillRect(50, 750, 1100, 100);

			let stringhold = textForPeople[currentCharacter][currentTalkSentence];

			textSlowed++;
			if (textSlowed > 3) {
				if (textPart < stringhold.length) {
					textPart++;
					charAudio[currentCharacter].play();

				}
				textSlowed = 0;
			}

			context.fillStyle = "White";
			context.fillText(stringhold.substring(0, textPart), 100, 800);
		}
	}
}

function animate() {
	if (walkCount == 0) {
		walkCycle = 0;
	} else if (walkCount < 15) {
		walkCycle = 4;
	} else if (walkCount < 30) {
		walkCycle = 0;
	} else if (walkCount < 45) {
		walkCycle = 8;
	} else if (walkCount < 60) {
		walkCycle = 0;
	} else {
		walkCount = 0;
	}
}


function checkCollision()
{
	if (Math.sqrt((fruitX + 64 - jiX - 128) * (fruitX + 64 - jiX - 128) + (fruitY + 64 - jiY - 128) * (fruitY + 64 - jiY - 128)) <= 128) {
		fruitX = (Math.random() * canvas.getBoundingClientRect().width) + 0;
		fruitY = (Math.random() * canvas.getBoundingClientRect().height) + 0;
		score++;
		switch (Math.floor(Math.random() * 4)) {
			case 0:                                                                                                                            
				fruitCol = 0;
				fruitRow = 0;
				break;
			case 1:
				fruitCol = 128;
				fruitRow = 0;
				break;
			case 2:
				fruitCol = 128;
				fruitRow = 128;
				break;
			case 3:
				fruitCol = 0;
				fruitRow = 128;
				break;
		}
    }
}
function makeRot() {

	let newVX = (charX[currentCharacter] - jiX) + 256;
	let newVY = (charY[currentCharacter] - jiY) + 256;

	newVX -= (600);
	newVY -= (700);

	pointerRingRot = (Math.atan2(newVY, newVX) * 180 / Math.PI);
}
function run()
{
	if (currentCharacter >= noOfCharacters) {
		console.log("win!");
		renderEnd();
	}
	else {
		if (!gameOver) {

			perSecCount++;
			if (perSecCount >= 59) {
				perSecCount = 0;
				perSec();
			}


			if (!talking && !choosing) {
				animate();
				move();
				makeRot();
			}

			//Cat:
			//m, mnj 


			render();
			//checkCollision();
		}
		else {
			audionew.pause();
		}

		if (currentTime >= startTime) {
			if (!gameOver) {
				gameOver = true;
				console.log("Gamer OVER");
				render();
			}
		}
	}

	window.requestAnimationFrame(run);
}

window.addEventListener('keyup', input);
window.addEventListener('keydown', input);

canvas.addEventListener("click", onClick, false);
canvas.addEventListener('touchstart', onClick);

function onClick(e) {

	talk();
}

window.requestAnimationFrame(run);
//setInterval(run, 10);


let perSecCount = 0;
function perSec()
{
	//localStorage.setItem("currentCar", currentCharacter);
	//localStorage.setItem("jiXHold", jiX);
	//localStorage.setItem("jiYHold", jiY);
	//localStorage.setItem("timeLeft", currentTime);
	audionew.play();

	currentTime++;
}

//removes the press hold menu
window.oncontextmenu = function (event) {
	event.preventDefault();
	event.stopPropagation();
	return false;
};